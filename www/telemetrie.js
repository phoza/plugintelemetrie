/*global cordova, module*/

module.exports = {
    startMeasurement: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Telemetrie", "startMeasurement", [name]);
    }
};
