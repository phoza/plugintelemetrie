package cz.team42.telemetry.plugin;

        import org.apache.cordova.*;
        import org.json.JSONArray;
        import org.json.JSONException;
        import android.os.Bundle;
        import android.os.Handler;
        import android.os.Message;
        import android.util.Log;
        import android.view.View;
        import cz.team42.telemetry.lib.dto.MeasurementResult;
        import cz.team42.telemetry.lib.services.MeasurementService;
        import cz.team42.telemetry.lib.services.PBTC.PBTCBluetoothService;
        import cz.team42.telemetry.lib.services.PendingMeasurement;

public class Telemetrie extends CordovaPlugin {

    private static final int MEASUREMENT_DONE = 0;

    private Handler handler;


    @Override
    public boolean execute(String action, JSONArray data, final CallbackContext callbackContext) throws JSONException {

/*        handler = new Handler(this.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MEASUREMENT_DONE:
                        MeasurementResult result = (MeasurementResult) msg.obj;
                        txtLog.append("Měření skončeno.\n");
                        txtLog.append(result.toString() + "\n");
                        btnStart.setEnabled(true);
                        break;
                }
            }
        };*/


        if (action.equals("startMeasurement")) {

            try {

                MeasurementService service = new PBTCBluetoothService();
                //txtLog.append("Čekám na výsledek měření...\n");

                PendingMeasurement serviceTask =  service.measure(new MeasurementService.Callback() {
                    @Override
                    public void measurementDone(MeasurementResult result) {
                        Log.i("result", result.toString());
                        //callbackContext.success("PEP 0" + result.toString());
                        //handler.obtainMessage(MEASUREMENT_DONE, result).sendToTarget();

                    }
                });

                synchronized (serviceTask){
                    try {
                        serviceTask.wait();
                        callbackContext.success("PEP 0" + serviceTask.getResult());
                        return true;

                    } catch (InterruptedException e) {
                        // Happens if someone interrupts your thread.
                        return false;
                    }
                }

            } catch (Exception e) {
                Log.e("KubaActivity", "Error starting measurement", e);
                //txtLog.append("Chyba při měření: " + e.getClass() + "/" + e.getMessage());
                //btnStart.setEnabled(true);
                callbackContext.error("PEPA 2");
                return false;
            }





        } else {
            callbackContext.error("PEPA 3");
            return false;

        }
    }
}
